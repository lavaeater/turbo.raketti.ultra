package com.lavaeater.controllers;

import com.kennycason.gdx.controller.controls.Controls;

/**
 * Created by 78899 on 2017-08-07.
 */

public enum GameControls implements Controls {
    DPAD_UP,
    DPAD_DOWN,
    DPAD_LEFT,
    DPAD_RIGHT,

    START,
    SELECT,

    A,
    B,
    X,
    Y,

    L1,
    L2,
    R1,
    R2
}
