package com.lavaeater.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.physics.box2d.Body

/**
 * Created by barry on 12/8/15 @ 10:29 PM.
 */
class BodyComponent(val body: Body) : Component