package com.lavaeater.components

import com.badlogic.ashley.core.Component
import com.lavaeater.Player

/**
 * Created by tommie on 2017-07-18.
 */
class ScoreComponent(val player: Player) : Component