package com.lavaeater.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.graphics.g2d.TextureRegion

/**
 * Created by barry on 12/8/15 @ 8:30 PM.
 */
class TextureComponent(val region: TextureRegion) : Component
