package com.lavaeater

import com.badlogic.gdx.controllers.Controller

class ControllerInfo(val isKeyBoardController: Boolean = false, val controller: Controller? = null)