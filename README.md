# README #



Hey there!

This repository is for a re-implementation of the classic Amiga game Turbo Raketti which in turn was a clone / homage of another classic, Gravity Force, which in turn might be inspired by Thrust or something or another.

Someone called it a "tunnel flyer", which might fit as well.

So, what is going on? Well, I'm not a game developer at all. I like the idea though.

I had this idea for a crazily ambitious game with a procedurally generated world, inspired by a drawing I made of 
a robot and my two daughters a while back. I started implementing that game using a framework for .NET called Otter2D (a fine framework if you ask me). After a while I got a bit bogged down by the fact that no one else seemed to be doing much with that engine and as far as I remember / understand, Kyle, the guy who made it, was moving on to NEZ / FNA or something like that.

Well, I tried that as well - but there was very little community action there as well (that I could easily find).

So, what was I supposed to do?

Well, Libgdx is a fine framework as well, lots of documentation, tutorials, books and a "thriving" community.

But it's Java.

I don't like Java. I'm a .NET-developer, I really like C# and everytime I try to do something in Java I just get sad. Getters, setters, it's just dull somehow. Now, if you love Java, that's great and I suspect it's a fine language and that I'm just getting to old. I like learning new languages, but with Java it just feels like giving up a bunch of fine features that C# has.

Anyways, enough of the language wars.

There's this new kid on the block called Kotlin. It compiles to bytecode, is supported by Android Studio and Google and is made by the good folks over at JetBrains (I have used Resharper for then years plus). 

And there's a re-implementation / wrapper or whatever for LibGdx called KTX for it. So...

A match made in heaven, yeah?

## Contact

Hit me up on twitter, I guess? @made_of_stars 

### How do I get set up? ###

I have set this project up on exactly two machines. You need Android studio or IntelliJ IDEA, I guess, and then clone it. Configure some run configuration for desktop. It probably won't even run on android or web, I just use desktop but I've left those configs in for future something something. We'll see. And then just run it. It supports 4 players and Xbox 360-controllers. Keyboard is WAD and space (turn, accelerate, fire). Controller is Left Analog, A for acceleration and right trigger to fire.

You need to install the Kotlin support in Android studio, I suppose?

The textures I've packed with TexturePacker, the physics bodies with PhysicsEditor and the (shoddy) particle effects with the built-in particle editor. 

## License

This code is released under the MIT license. Checkout license.txt for details. You can do basically what you want with it. Cheers.

### The Game and Everything ###

I wanted to make a game using Box2D physics and the Ashley ECM-system. Why? Someone else said it was a good idea.

I was basically done with everything when I realized that Aurelien Ribons physics body editor was a bit to rough around the edges for my weak sensibilities so I changed to the Physics Editor and texture packer. When trying to add a map and stuff, everything was all over the place and I basically had to rewrite the entire rendering system.

Everything in this is based around systems and components. The entities are just base entities with a bunch of components for sprites, health, collisions etc tacked on. 

## Whats next?

I'm not sure. Better effects? I've don'em meself, but I have no patience. Their placing and such is a bit off. I'm not sure why. I use a statemachine I just copied in from some repo instead of a reference. This is meant as a form of "reference implementation" for some other poor noob to use as a starter for his / her own probject.

## Systems ##

### AnimationSystem

Not used at the moment and doesn't do anything at the moment.

### CollisionSystem

Is both the contact listener for the Box2D world and also an IteratingSystem for entities with a collision component. When two bodies collide I check what they are. If at least one is a ship it gets a collision component which then is used in the processEntity method to set scores, take damage and stuff 

### DebugSystem

This *was* used to print out some debug information about positions and stuff.

### FollowCameraSystem

A simple system that sets the camera position over a specific entity. Not used as of now, see MultiFollowCameraSystem below

### GamepadInputSystem

A system for handling gamepad input. If someone is using a gamepad, this system is set as the listener for the controller and in it we poll all the controllers to see if their axes are being moved and also listen for button presses. If anything like that is happening, we change the StateComponent for that entity.

### GameStateSystem

Checks if all but one player is dead, if so calls the game-over method which in turn changes the game state to go back to the splash screen

### HealthSystem

Checks if all the players are alive, all the time. Might not be the best architecture, I'm open for suggestions. Anyways, if a player is dead, we put a particle effect of an explosion at its position

### KeyboardInputSystem

Input for keyboard.

### MultiFollowCameraSystem

I'm a little proud of this one. It's not tweaked enought just yet, I'm gonna get back to it. Anyways, it makes sure to zoom the camera so that all player entities are visible at all time. If they are close by each other, it zooms in, and if they are far apart, it zooms out. 

### PhysicsDebugSystem

Just a regular Physics Debug renderer for Box2D, you've seen this before.

### PhysicsSystem

Just steppin' the world and stuff. 

### PlayerProjectileSystem

Creates missiles if the statecomponent for a player is in state "isFirin". Makes sure there is a rate of fire as well.

### ProjectileMovementSystem

Before I figured out LinearVelocity I applied some thrust to the missiles, not anymore.

### RemovalSystem

Removes an entity and physics body. Happens to bullets all the time, and ships when they have low health.


### RenderingSystem

Renders sprites and stuff.

### ZComparator

To keep track of z-stuff, not really used just yet, it was from some tutorial.

### ZPositionComparator

Why are there two basically identical pieces of code here?